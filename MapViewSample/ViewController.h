//
//  ViewController.h
//  MapViewSample
//
//  Created by Gaurav Rastogi on 02/04/15.
//  Copyright (c) 2015 Gaurav Rastogi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController : UIViewController <MKMapViewDelegate,CLLocationManagerDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mpView;
@property (strong, nonatomic) CLLocationManager *locationManager;


@property (strong, nonatomic)CLLocation *myCurrentLocation;

- (IBAction)onClickOfSegmentedControl:(UISegmentedControl *)sender;

- (IBAction)onClickOfAddPinButton:(UIButton *)sender;

- (IBAction)onClickOfShowRegionButton:(UIButton *)sender;
@end

