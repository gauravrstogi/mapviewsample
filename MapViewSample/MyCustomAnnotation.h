//
//  MyCustomAnnotation.h
//  MapViewSample
//
//  Created by Gaurav Rastogi on 03/04/15.
//  Copyright (c) 2015 Gaurav Rastogi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyCustomAnnotation : NSObject <MKAnnotation>

@property (nonatomic,copy)NSString *title;
@property (nonatomic,readonly)CLLocationCoordinate2D coordinate;

-(instancetype)initWithTitle:(NSString *)title coordinates:(CLLocationCoordinate2D )locationCoordinates;

-(MKAnnotationView *)annotationView;

@end
