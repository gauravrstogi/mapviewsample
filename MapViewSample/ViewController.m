//
//  ViewController.m
//  MapViewSample
//
//  Created by Gaurav Rastogi on 02/04/15.
//  Copyright (c) 2015 Gaurav Rastogi. All rights reserved.
//

#import "ViewController.h"
#import "MyCustomAnnotation.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize mpView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    
    if (self.locationManager && [self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }
    
    //[self.locationManager startUpdatingLocation];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CLLocationManager Delegate Methods -
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    NSLog(@"locations = %@",locations);
    
    self.myCurrentLocation = [locations firstObject];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
}

#pragma mark - MKMapView Delegate Methods -

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MyCustomAnnotation class]]) {
        MyCustomAnnotation *myAnnotation = (MyCustomAnnotation *)annotation;
        
        MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"MyCustomAnnotation"];
        
        if (annotationView==nil)
            annotationView = myAnnotation.annotationView;
        else
            annotationView.annotation = annotation;
        
        return annotationView;
    }
    
    return nil;
}



-(MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKCircle class]]) {
        MKCircleView *circleView = [[MKCircleView alloc] initWithCircle:(MKCircle*)overlay];
        circleView.fillColor = [[UIColor grayColor] colorWithAlphaComponent:0.5];
        circleView.strokeColor = [[UIColor whiteColor] colorWithAlphaComponent:0.7];
        circleView.lineWidth = 2;
        return circleView;
    }
    else if ([overlay isKindOfClass:[MKPolygon class]])
    {
        MKPolygonView *polyView = [[MKPolygonView alloc] initWithPolygon:(MKPolygon*)overlay];
        polyView.fillColor = [[UIColor cyanColor] colorWithAlphaComponent:0.2];
        polyView.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.7];
        polyView.lineWidth = 3;
        return polyView;
    }
    
    return nil;
}

- (IBAction)onClickOfSegmentedControl:(UISegmentedControl *)sender {
    
    switch (sender.selectedSegmentIndex) {
        case 0:
            mpView.mapType = MKMapTypeStandard;
            break;
        case 1:
            mpView.mapType = MKMapTypeSatellite;
            break;
        case 2:
            mpView.mapType = MKMapTypeHybrid;
        default:
            
            break;
    }
}

- (IBAction)onClickOfAddPinButton:(UIButton *)sender {
    
    
    
    
    CLLocationCoordinate2D cementeryCoordinates = CLLocationCoordinate2DMake(47.879854, 10.641000);
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(cementeryCoordinates, 2500.0, 2500.0);
    MKCoordinateRegion adjustedRegion = [mpView regionThatFits:region];
    [mpView setRegion:adjustedRegion animated:YES];
    
    
    
    
    
    
    /*Adding MyCustomAnnotation to the MapView*/
    MyCustomAnnotation *customAnnotation = [[MyCustomAnnotation alloc] initWithTitle:@"Mt Zion Cemetery" coordinates:cementeryCoordinates];
    [mpView addAnnotation:customAnnotation];
    
    
    
    
    
    
    
    /*Adding PointAnnotation to the MapView*/
    MKPointAnnotation *pointAnnotation = [[MKPointAnnotation alloc] init];
    pointAnnotation.title  = @"Mt Zion Cemetery";
    pointAnnotation.coordinate = cementeryCoordinates;
    //Drop pin on map
    [mpView addAnnotation:pointAnnotation];
    
    
    
    
    
    
    /*Adding CircleOverlay to the MapView*/
    MKCircle *circle = [MKCircle circleWithCenterCoordinate:cementeryCoordinates radius:1000.0];
    circle.title = @"My Circle Overlay";
    [mpView addOverlay:circle];
    
    
    
    
    
    
    /*Adding PolygonOverlay to the MapView*/
    CLLocationCoordinate2D  points[4];
    points[0] = CLLocationCoordinate2DMake(47.879854, 10.641000);
    points[1] = CLLocationCoordinate2DMake(47.889854, 10.642000);
    points[2] = CLLocationCoordinate2DMake(47.875854, 10.631000);
    points[3] = CLLocationCoordinate2DMake(47.869854, 10.621000);
    MKPolygon* polygon = [MKPolygon polygonWithCoordinates:points count:4];
    polygon.title = @"My Polygon Overlay";
    [mpView addOverlay:polygon];
    
    
    
}

- (IBAction)onClickOfShowRegionButton:(UIButton *)sender {
    
    //set location and zoom level
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(self.myCurrentLocation.coordinate, 2500.0, 2500.0);
    MKCoordinateRegion adjustedRegion = [mpView regionThatFits:viewRegion];
    [mpView setRegion:adjustedRegion animated:YES];
    
    [self.locationManager stopUpdatingLocation];
    
}


@end
