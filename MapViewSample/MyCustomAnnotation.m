//
//  MyCustomAnnotation.m
//  MapViewSample
//
//  Created by Gaurav Rastogi on 03/04/15.
//  Copyright (c) 2015 Gaurav Rastogi. All rights reserved.
//

#import "MyCustomAnnotation.h"

@implementation MyCustomAnnotation 

-(instancetype)initWithTitle:(NSString *)title coordinates:(CLLocationCoordinate2D )locationCoordinates{

    self = [super init];
    
    if (self) {
        _title = title;
        _coordinate = locationCoordinates;
    }
    
    return self;
}


-(MKAnnotationView *)annotationView{
    
    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"MyCustomAnnotation"];
    annotationView.enabled = TRUE;
    annotationView.canShowCallout = TRUE;
    annotationView.image = [UIImage imageNamed:@"yellowPinImage"];
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    return annotationView;
}

@end
